# INSTALLATION


## Before you begin
1. If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

2. Install composer-asset-plugin needed for yii assets management
```bash
composer global require "fxp/composer-asset-plugin"
```

### Get source code
#### Clone repository manually
```
git clone https://misaniuk@bitbucket.org/misaniuk/marker.git
```
#### Install composer dependencies
```
composer install
```
#### Import migrations
```
php console/yii migrate/up
```

## Demo data
### Demo Users
```
Login: webmaster
Password: webmaster

Login: manager
Password: manager

Login: user
Password: user
```
