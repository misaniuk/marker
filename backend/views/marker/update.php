<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Marker */

$this->title = 'Update Marker: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Markers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="marker-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
