<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MarkerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Markers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marker-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Marker', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'country',
            'city',
            'address',
            'place_type:ntext',
            // 'marker_icon',
            // 'lon:ntext',
            // 'lat:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
