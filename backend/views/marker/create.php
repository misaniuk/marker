<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Marker */

$this->title = 'Create Marker';
$this->params['breadcrumbs'][] = ['label' => 'Markers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marker-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
