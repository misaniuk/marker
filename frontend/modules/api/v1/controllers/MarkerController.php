<?php
namespace frontend\modules\api\v1\controllers;

use common\models\Marker;
use common\models\MarkerSearch;
use Yii;
use frontend\modules\api\v1\resources\Article;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ArticleController
 * @author Eugene Terentev <eugene@terentev.net>
 */
class MarkerController extends ActiveController
{
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    /**
     * @var string
     */
    public $modelClass = 'common\models\Marker';
    /**
     * @var array
     */
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items'
    ];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        $actions['nearby'] = [
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'prepareDataProvider' =>  function ($action) {
                $params = Yii::$app->request->queryParams;
                if ($params['radius']) {
                    $condition = "POW({$params['lat']} - lat, 2) + POW({$params['lon']} - lon, 2) <= POW({$params['radius']}, 2)";

                } else {
                    $condition = "";
                }
                return new ActiveDataProvider([
                    'query' => Marker::find()->where($condition)
                ]);
            }
        ];

        return $actions;
    }

    public function prepareDataProvider() {

        $searchModel = new MarkerSearch();
        return $searchModel->search(Yii::$app->request->queryParams);
    }


}
