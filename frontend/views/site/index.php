<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MarkerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Markers';
?>
<div class="marker-index">

    <div id="map"></div>
    <input id="pac-input" class="controls" type="text" placeholder="Что искать?">

    <?= Html::dropDownList("place_type", null, $place_types, [
        'prompt' => 'Тип места',
        'id'=>'place_types',
        'class' => 'controls'
    ]) ?>
    <?= Html::dropDownList("country", null, $countries, [
        'prompt' => 'Страна',
        'id'=>'countries',
        'class' => 'controls'

    ]) ?>
    <input id="radius" class="controls" type="text" placeholder="Радиус">

</div>
<title>Places Searchbox</title>