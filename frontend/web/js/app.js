function initMap() {
    navigator.geolocation.getCurrentPosition(function(position){
        window.markers = [];

        //add marker to map
        function addMarkers(markers) {
            $.each(markers.items, function(i, e) {
                var lat = parseFloat(e.lat);
                var lon = parseFloat(e.lon);
                var icon = "http://storage.marker.dev/images/" + e.marker_icon + ".png";
                var marker = new google.maps.Marker({
                    position: {lat: lat, lng: lon},
                    map: map,
                    icon: icon,

                });

                window.markers.push(marker);

            });
        }

        //remove all markers from map
        function removeMarkers() {
            for(i=0; i<window.markers.length; i++){
                window.markers[i].setMap(null);
            }
        }
        lat = position.coords.latitude;
        lng = position.coords.longitude;

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat, lng: lng},
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        //add marker at user position
        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,

        });

        //get all markers
        $.get("/api/v1/marker", function(markers) {
            addMarkers(markers);
        });

        //country, place_type filtering
        $("select#place_types, select#countries").change(function(){
            var place_type = $("select#place_types").val();
            var country = $("select#countries").val();
            $.get("/api/v1/marker?MarkerSearch[place_type]=" + place_type + "&MarkerSearch[country]=" + country, function(markers) {

                removeMarkers();
                addMarkers(markers);
            });
        });

        //search markers by radius
        $("input#radius").keyup(function(){
            var radius = $("input#radius").val();
            var url = "http://marker.dev/api/v1/marker/nearby?lat=51.24094319999999&lon=33.20505209999993&radius=" + radius;
            $.get(url, function(markers){

                removeMarkers();
                addMarkers(markers);
            });
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var countries = document.getElementById('pac-countries');
        var place_types = document.getElementById('place_types');
        var countries = document.getElementById('countries');
        var radius = document.getElementById('radius');

        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(place_types);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(countries);
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(radius);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {

                // Get Marker info
                var marker = {
                    country: "",
                    city: "",
                    address: place.formatted_address,
                    lng: place.geometry.location.lng(),
                    lat: place.geometry.location.lat(),
                };

                place.address_components.forEach(function(components) {
                    components.types.forEach(function(types) {
                        if (types == "country")
                        {
                            marker.country = components.long_name;
                        } else if (types == "locality") {
                            marker.city = components.long_name;
                        }
                    });
                });
                console.log(place);
                console.log(marker);

                //set marker fields
                $("#marker-country").val(marker.country);
                $("#marker-city").val(marker.city);
                $("#marker-address").val(marker.address);
                $("#marker-lon").val(marker.lng);
                $("#marker-lat").val(marker.lat);

                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
        // [END region_getplaces]
    });



}



