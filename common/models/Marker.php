<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "marker".
 *
 * @property integer $id
 * @property string $country
 * @property string $city
 * @property string $address
 * @property string $place_type
 * @property string $marker_icon
 * @property string $lon
 * @property string $lat
 */
class Marker extends \yii\db\ActiveRecord
{
    public static $PLACE_TYPES = [
        "hospital" => "Госпиталь",
        "library" => "Библиотека",
        "cafe" => "Кафе",
    ];

    public static $PLACE_ICONS = [
        "hospital" => "hospital icon",
        "library" => "library icon",
        "cafe" => "cafe icon",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'city', 'address', 'place_type', 'marker_icon', 'lon', 'lat'], 'required'],
            [['place_type', 'lon', 'lat'], 'string'],
            [['country', 'city', 'marker_icon'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'city' => 'City',
            'address' => 'Address',
            'place_type' => 'Place Type',
            'marker_icon' => 'Marker Icon',
            'lon' => 'Lon',
            'lat' => 'Lat',
        ];
    }

    /**
     * @inheritdoc
     * @return MarkerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MarkerQuery(get_called_class());
    }
}
