<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Marker;

/**
 * MarkerSearch represents the model behind the search form about `common\models\Marker`.
 */
class MarkerSearch extends Marker
{
    public $radius;
    public $userLat;
    public $userLon;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['country', 'city', 'address', 'place_type', 'marker_icon', 'lon', 'lat','radius','userLat','userLon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

//        var_dump($params);
//        var_dump(Yii::$app->request->cookies);
        $query = Marker::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'place_type', $this->place_type])
            ->andFilterWhere(['like', 'marker_icon', $this->marker_icon])
            ->andFilterWhere(['like', 'lon', $this->lon])
            ->andFilterWhere(['like', 'lat', $this->lat]);
//            ->andWhere("POW(3 - lat, 2) + POW(4 - lon, 2) <= POW({$this->radius}, 2)");

        return $dataProvider;
    }
}
