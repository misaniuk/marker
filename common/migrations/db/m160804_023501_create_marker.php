<?php

use yii\db\Migration;

class m160804_023501_create_marker extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('marker', [
            'id' => $this->primaryKey(),
            'country' => $this->string(100),
            'city' => $this->string(100),
            'address' => $this->string(300),
            'place_type' => $this->string(50),
            'marker_icon' => $this->string(100),
            'lon' => $this->text()->notNull(),
            'lat' => $this->text()->notNull()
        ], $tableOptions);

    }

    public function down()
    {
        echo "m160804_023501_create_marker cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
